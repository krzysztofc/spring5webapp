package springframework.demospring5webapp.repositories;

import org.springframework.data.repository.CrudRepository;
import springframework.demospring5webapp.model.Author;

public interface AuthorRepository extends CrudRepository<Author,Long> {
}
