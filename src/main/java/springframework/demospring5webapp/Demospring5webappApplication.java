package springframework.demospring5webapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Demospring5webappApplication {

    public static void main(String[] args) {

        SpringApplication.run(Demospring5webappApplication.class, args);
    }
}
